﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
//using System.Windows.Input;

namespace ListBoxesWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            RestaurantList.ItemsSource = RestaurantModel.Restaurants;
        }

        private void SwitchStyle(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var combobox = sender as ComboBox;
            if (combobox == null || RestaurantList == null) return;

            Style = null;
            RestaurantList.Style = null;

            switch (combobox.SelectedIndex)
            {
                case 1:
                    RestaurantList.Style = GetStyle("List-Columns");
                    break;
                case 2:
                    RestaurantList.Style = GetStyle("List-Grid");
                    break;
            }
        }

        private static Style GetStyle(string key)

        {
            var resource = Application.Current.FindResource(key);
            return resource as Style;
        }

        private void RestaurantList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int index = RestaurantList.SelectedIndex;
            if (index != -1)
            {
                MessageBox.Show(index.ToString());
            }
        }
    }
}
